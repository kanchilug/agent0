# Getting Environment Variables for Co-Ordinator Details
import os

coordinator_email = os.environ["COORDINATOR_EMAIL"]
coordinator_name = os.environ["COORDINATOR_NAME"]


def set_agenda():
    # Mention the agenda if there is no scheduled talks
    return f""""""


def mc_weeklydiscussion(date):
    AGENDA = set_agenda()
    message = f"""
Subject: [KanchiLUG] Weekly discussion - {date}

Hi everyone,
This week in KanchiLUG we have scheduled an weekly discussion as online meeting on Sunday, {date} 17:00 - 18:00 IST

Meeting link : https://meet.jit.si/KanchiLugWeeklyDiscussion
{AGENDA}
Weekly discussion is an open and friendly discussion where topics related to Linux/FOSS technologies will be discussed. We will meet in the online jitsi meeting and discuss new linux things everyone explored this week and we chat about linux news and topics. If you are facing any issues with linux or any FOSS applications, you can also share your issues during the discussion. Our KanchiLUG community will help to debug or suggest some good alternatives.

Can join with any browser or JitSi android app.
All the Discussions are in Tamil.

About KanchiLUG : Kanchi Linux Users Group [ KanchiLUG ] has been spreading awareness on Free/Open Source Software (F/OSS) in Kanchipuram since November 2006.

Anyone can join! (Entry is free)
Everyone is welcome
Feel free to share this to your friends

--
Thanks and Regards,
{coordinator_name}
KanchiLUG Co-ordinator
Personal Email: {coordinator_email}
Mailing list: kanchilug@freelists.org
Repository : https://gitlab.com/kanchilug
Twitter handle: @kanchilug
"""
    return message


def bc_weeklydiscussion(date):
    AGENDA = set_agenda()
    message = f"""
Subject: Weekly discussion - {date}

Hi everyone,
This week in <b>KanchiLUG</b> we have scheduled an weekly discussion as online meeting on <b>Sunday, {date} 17:00  -  18:00 IST</b>

<a href="https://meet.jit.si/KanchiLugWeeklyDiscussion" target="_blank">Meeting Link</a>
{AGENDA}
Weekly discussion is an open and friendly discussion where topics related to Linux/FOSS technologies will be discussed. We will meet in the online jitsi meeting and discuss new linux things everyone explored this week and we chat about linux news and topics. If you are facing any issues with linux or any FOSS applications, you can also share your issues during the discussion. Our KanchiLUG community will help to debug or suggest some good alternatives.

Can join with any browser or JitSi android app.
All the Discussions are in Tamil.
"""
    return message


def fc_weeklydiscussion(date):
    AGENDA = set_agenda()
    message = f"""
<h2>KanchiLUG's Weekly discussion - {date}</h2>

Hi everyone,
This week in <b>KanchiLUG</b> we have scheduled an weekly discussion as online meeting on <b>Sunday, {date} 17:00  -  18:00 IST</b>

<a href="https://meet.jit.si/KanchiLugWeeklyDiscussion" target="_blank">Meeting Link</a>
{AGENDA}
Weekly discussion is an open and friendly discussion where topics related to Linux/FOSS technologies will be discussed. We will meet in the online jitsi meeting and discuss new linux things everyone explored this week and we chat about linux news and topics. If you are facing any issues with linux or any FOSS applications, you can also share your issues during the discussion. Our KanchiLUG community will help to debug or suggest some good alternatives.

Can join with any browser or JitSi android app.
All the Discussions are in Tamil.
"""
    return message


def talks_msg_generator(talk_list):
    talks_msg = """
    """
    counter = 0
    for item in talk_list:
        msg = f"""
Talk {counter}:
Topic : {item.topic}
Description : {item.description} 
Duration : {item.estduration}
Name : {item.name}
About : {item.about}
"""
        talks_msg += msg
        counter += 1
    return talks_msg


def talks_msg_generator_html(talk_list):
    talks_msg = """
    """
    counter = 0
    for item in talk_list:
        msg = f"""
<b>Talk {counter}:</b>
<b>Topic :</b> {item.topic}
<b>Description :</b> {item.description} 
<b>Duration :</b> {item.estduration}
<b>Name :</b> {item.name}
<b>About :</b> {item.about}
"""
        talks_msg += msg
        counter += 1
    return talks_msg


def mc_monthlymeeting(date, talks_msg):
    top_content = f"""
Subject: [KanchiLUG] Monthly Meeting - {date}

Hi everyone,
KanchiLUG's Monthly meet is scheduled as online meeting this week on Sunday, {date} 17:00 - 18:00 IST

Meeting link : https://meet.jit.si/KanchiLugMonthlyMeet

Can join with any browser or JitSi android app.
All the Discussions are in Tamil.

Talk Details"""
    footer = f"""
After Talks : Q&A, General discussion

About KanchiLUG : Kanchi Linux Users Group [ KanchiLUG ] has been spreading awareness on Free/Open Source Software (F/OSS) in Kanchipuram since November 2006.

Anyone can join! (Entry is free)
Everyone is welcome
Feel free to share this to your friends
--
Thanks and Regards,
{coordinator_name}
KanchiLUG Co-ordinator
Personal Email: {coordinator_email}
Mailing list: kanchilug@freelists.org
Repository : https://gitlab.com/kanchilug
Twitter handle: @kanchilug
    """
    message = top_content + talks_msg + footer
    return message


def bc_monthlymeeting(date, talks_msg):
    top_content = f"""
Subject: Monthly Meeting - {date}

Hi everyone,
KanchiLUG's Monthly meet is scheduled as online meeting this week on <b>Sunday, {date} 17:00 - 18:00 IST</b>

<a href="https://meet.jit.si/KanchiLugMonthlyMeet" target="_blank">Meeting Link</a>

Can join with any browser or JitSi android app.
All the Discussions are in Tamil.

<b>Talk Details</b>"""
    footer = """
See you guys in the meet!"""
    message = top_content + talks_msg + footer
    return message


def fc_monthlymeeting(date, talks_msg):
    top_content = f"""
# KanchiLUG's Monthly Meeting - {date}
Hi everyone,
KanchiLUG's Monthly meet is scheduled as online meeting this week on <b>Sunday, {date} 17:00 - 18:00 IST</b>

<a href="https://meet.jit.si/KanchiLugMonthlyMeet" target="_blank">Meeting Link</a>

Can join with any browser or JitSi android app.
All the Discussions are in Tamil.

<b>Talk Details</b>"""
    footer = """
See you guys in the meet!"""
    message = top_content + talks_msg + footer
    return message


def mc_callforspeakers(date):
    message = f"""
Subject: [KanchiLUG] Call For Speakers- {date}

Hi All,
Kanchi Linux Users Group, Kanchipuram [ KanchiLUG ] has been spreading awareness on Free/Open Source Software (F/OSS) in Kanchipuram since November 2006

KanchiLUG Monthly meet is going to be held on upcoming {date} through an online meeting platform called Jitsi meet at 17:00 IST.

We are inviting speakers to give a talk on things they have been working on FOSS Technologies like Linux, Free/Libre/Open Source Softwares/Technologies, Open Source Programming Languages/Frameworks etc.

This is an open stage for talking about open source. You can be at any experience level (from beginners to advanced) . We are very happy to schedule your session and listen to your open source experience.

Follow the below steps to give a talk in this week's meeting:
Step 1: If you are already a member of KanchiLUG mailing list you can skip this step1
If you are not a member of KanchiLUG mailing list then,
Go to this link : https://kanchilug.wordpress.com/join-mailing-list/
Follow the procedure mentioned in that link to join the mailing list.
(Note: You must be a member of the mailing list to send a mail to the mailing list)

Step 2: send a mail to kanchilug@freelists.org with below mentioned details
Topic:
Description:
Duration:
Your Name:
About Yourself:

You will get a confirmation mail within 24 hrs.
Note: If you haven't received any reply mail within 24 hrs, you can remind me in my personal mail id : {coordinator_email}

Entry is free!
Feel free to share this to your friends

--
Thanks and Regards,
{coordinator_name}
KanchiLUG Co-ordinator
Personal Email: {coordinator_email}
Mailing list: kanchilug@freelists.org
Repository : https://gitlab.com/kanchilug
Twitter handle: @kanchilug
    """
    return message


def bc_callforspeakers(date):
    message = f"""
Subject: Call For Speakers - {date}

Hi All,
Kanchi Linux Users Group, Kanchipuram [ KanchiLUG ] has been spreading awareness on Free/Open Source Software (F/OSS) in Kanchipuram since November 2006

<b>KanchiLUG Monthly meet</b> is going to be held on upcoming <b>{date}</b> through an online meeting platform called <b>Jitsi meet</b> at <b>17:00 IST</b>.

We are inviting speakers to give a talk on things they have been working on FOSS Technologies like Linux, Free/Libre/Open Source Softwares/Technologies, Open Source Programming Languages/Frameworks etc.

This is an open stage for talking about open source. You can be at any experience level (from beginners to advanced) . We are very happy to schedule your session and listen to your open source experience.

<b>Follow the below steps to give a talk in this week's meeting:</b>
<b>Step 1:</b> If you are already a member of KanchiLUG mailing list you can skip this step1
If you are not a member of KanchiLUG mailing list then,
Go to this <a href="https://kanchilug.wordpress.com/join-mailing-list/" target="_blank">Link</a>
Follow the procedure mentioned in that link to join the mailing list.
<em>(Note: You must be a member of the mailing list to send a mail to the mailing list)</em>

<b>Step 2:</b> send a mail to kanchilug@freelists.org with below mentioned details
<b>Topic:
Description:
Duration:
Your Name:
About Yourself:
</b>
You will get a confirmation mail within 24 hrs.
Note: If you haven't received any reply mail within 24 hrs, you can remind me in my personal mail id : {coordinator_email}

Entry is free!
Feel free to share this to your friends
"""
    return message


def fc_callforspeakers(date):
    message = f"""
# Call For Speakers - {date}

Hi All,
Kanchi Linux Users Group, Kanchipuram [ KanchiLUG ] has been spreading awareness on Free/Open Source Software (F/OSS) in Kanchipuram since November 2006

<b>KanchiLUG Monthly meet</b> is going to be held on upcoming <b>{date}</b> through an online meeting platform called <b>Jitsi meet</b> at <b>15:00 IST</b>.

We are inviting speakers to give a talk on things they have been working on FOSS Technologies like Linux, Free/Libre/Open Source Softwares/Technologies, Open Source Programming Languages/Frameworks etc.

This is an open stage for talking about open source. You can be at any experience level (from beginners to advanced) . We are very happy to schedule your session and listen to your open source experience.

<b>Follow the below steps to give a talk in this week's meeting:</b>
<b>Step 1:</b> If you are already a member of KanchiLUG mailing list you can skip this step1
If you are not a member of KanchiLUG mailing list then,
Go to this <a href="https://kanchilug.wordpress.com/join-mailing-list/" target="_blank">Link</a>
Follow the procedure mentioned in that link to join the mailing list.
<em>(Note: You must be a member of the mailing list to send a mail to the mailing list)</em>

<b>Step 2:</b> send a mail to kanchilug@freelists.org with below mentioned details
<b>Topic:
Description:
Duration:
Your Name:
About Yourself:
</b>
You will get a confirmation mail within 24 hrs.
Note: If you haven't received any reply mail within 24 hrs, you can remind me in my personal mail id : {coordinator_email}

Entry is free!
Feel free to share this to your friends
"""
    return message
