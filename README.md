# Agent0

### Agent0 is a automation script written by <i>Parameshwar Arunachalam</i>

It automates the process of sending regular meeting invites for montly,weekly events

It updates the status in multiple places like mailing list, Blog and the Tamil linux community forum 

For now Tamil linux community forum will be updated for Monthly meetings only.

If you are interested in contributing to the script, just fork it, update it and raise PR 

I tried to write this automation in a modular way so even individual python files can be re-used in your projects 

# Modules : 

## agent0
Agent0 is module orchestrator which having flows to complete your actions in CLI 
## content 
All Content templates preserved here 

* Call for Speaker
* Monthly Meet 
* Weekly Discussion 

change respective function as per the need 

## discourser
To update posts in forums this module can be used 
## mailer 
Mailing Utility sends mail to mailing lists Weekly Discussion, Monthly Meet, Call for Speakers 
## talks 
Generates the talks part of the content for blog, mailing list and discourse forum 

# Pre Requesites
* Python >= 3.8 

# Setup & Environment variable 
* install the dependencies via pip 
``` pip install -r requirements.txt```
* Clone the repository
* Create a python virtual environment for the project agent0 
* (Optional) Create a entry for bash alias for activating the environment with ease
* In the enviornment activation shell file 

``` <vir-environment>/bin/activate```

update the environment variable as bash variables and export them

for example
```
API_KEY1=<your-api-key>
API_KEY2=<your-api-key>
SECRET=<your-secret>
EMAIL=<your-email>

export API_KEY1
export API_KEY2
export SECRET
export EMAIL
```
this will load these environmental variables when you activate the the environment 
