import smtplib, ssl
import os
coordinator_email=os.environ["COORDINATOR_EMAIL"]
coordinator_name=os.environ["COORDINATOR_NAME"]
def send_mail(message, to_addr):
    port=465
    app_password=os.environ["GOOGLE_APP_PASSWORD"]
    
    context=ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as mysmtp:
        mysmtp.login(coordinator_email, app_password)
        mysmtp.sendmail(coordinator_email, to_addr ,message)


def to_mailinglist(message):
    to_addr = "kanchilug@freelists.org"
    message=f'''From: {coordinator_name} <{coordinator_email}>
To: KanchiLUG mailing list <{to_addr}>'''+message
    send_mail(message, to_addr)
    print("Mail sent to mailing list!")

def to_blog(message):
    to_addr = os.environ["WORDPRESS_EMAIL"] 

    message=f'''From: {coordinator_name} <{coordinator_email}>
To: KanchiLUG Blog <{to_addr}>'''+message
    send_mail(message, to_addr)
    print("Post has been sent!")
