import os
import requests


def to_tlc(message, topic, category):
    headers = {
        "Content-Type": "application/json",
        "Api-Key": os.environ["DISCOURSE_API_KEY"],
        "Api-Username": os.environ["DISCOURSE_API_USERNAME"],
    }
    data_req = {"raw": message, "category": category, "topic_id": topic}

    data = requests.post(
        "https://forums.tamillinuxcommunity.org/posts.json",
        headers=headers,
        json=data_req,
    )
    if data.status_code == 200:
        print("Post created successfully!")
    else:
        print("Something happened")
        print(data.status_code)
